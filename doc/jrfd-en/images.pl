# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/psfig{file=carte,scale=0.5};LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 39.64ex; vertical-align: -0.11ex; " SRC="|."$dir".q|img2.svg"
 ALT="\psfig{file=carte, scale=0.5}">|; 

$key = q/star;MSF=1.6;LFS=11;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.29ex; vertical-align: -0.11ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\star$">|; 

1;

