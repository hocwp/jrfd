const div = document.getElementById("dices");

div.style.position = "absolute";
div.style.top = "10px";
div.style.left = "10px";

div.innerHTML = `<button onclick='rollDices()'>Dices</button>
  <div id='result'></div>`;


function rollDices () {
	const res = document.getElementById("result");

	const rnd = Math.floor(Math.random() * 100);

	const unit = rnd % 10;
	const tens = Math.floor(rnd / 10);

	res.innerHTML = `
   <p>Dices: ${tens}${unit}</p>
   <p>
     <img src="../des/mauve/de_${tens}.png" width="25%"></img>
     <img src="../des/vert/de_${unit}.png" width="25%"></img>
   </p>
`;
}
