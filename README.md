# JRFD (Jeu de Rôle Fantaisiste et Démocratique - Fantasy and Democratic Role Playing Game)

## Rules

* You ask a question
* You choose a difficulty
* If there is more than one player, you can adjust the difficulty following the other players remarks (the democratic part).
* You roll the dices:
  * if the dices are greater or equal to the difficulty the answer to the question is YES
  * else the answer is NO
* And so on for the next player (or you)

The difficulty system can be done with a random number generator between 0 - 100 or 0 - 10 or 1 - 6 for standard dice or ...
1 is easy. 100 is impossible.

[Original version](http://hocwp.free.fr/jrfd/) /
[Enhanced version](http://hocwp.free.fr/jrfd-en/jrfd-en.html) ([pdf](https://gitlab.com/hocwp/jrfd/-/blob/master/doc/jrfd-en.pdf?ref_type=heads))/
[Enhanced version (fr)](http://hocwp.free.fr/jrfd-2024/jrfd.html) ([pdf](https://gitlab.com/hocwp/jrfd/-/blob/master/doc/jrfd.pdf?ref_type=heads))



## Usage

Here is a minimalist forth version:

```
gforth jrfd.fs
```

The syntax is:

`q: <your question> ? <difficulty 0 - 100> ??`

## Examples

```
$ gforth jrfd.fs
Gforth comes with ABSOLUTELY NO WARRANTY; for details type `license'
Type `bye' to exit

q: I'm on the moon ? 20 ??    23 / 20 Yes :-)
 ok

q: The floor is made of cheese ? 50 ??    4 / 50 No :-(
 ok

\ oops  ok

q: I died because I can't heat ? 20 ??    0 / 20 No :-(
 ok

\ ouf saved!!!  ok

q: There is martien around ? 50 ??    35 / 50 No :-(
 ok

q: I died because I was alone ? 5 ??    39 / 5 Yes :-)
 ok

bye
```

## Enhancements

Feel free to ask for merge request with your own story if you want to share it with others!
